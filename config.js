var fs = require("fs");

exports.init = function(){
  var localConfigFile, config;
  localConfigFile = "env-user.json";
  if(!fs.existsSync(localConfigFile)){
    return {
      trelloKey: process.env.TRELLO_KEY,
      trelloToken: process.env.TRELLO_TOKEN,
      trelloBoardId: process.env.TRELLO_BOARD_ID,
      bitbucketUser: process.env.BITBUCKET_USER,
      bitbucketRepo: process.env.BITBUCKET_REPO
    };
  }
  config = JSON.parse(fs.readFileSync(localConfigFile));
  return {
    trelloKey: process.env.TRELLO_KEY || config.trelloKey,
    trelloToken: process.env.TRELLO_TOKEN || config.trelloToken,
    trelloBoardId: process.env.TRELLO_BOARD_ID || config.trelloBoardId,
    bitbucketUser: process.env.BITBUCKET_USER || config.bitbucketUser,
    bitbucketRepo: process.env.BITBUCKET_REPO || config.bitbucketRepo
  };
};