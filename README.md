Bitbucket => Trello Bridge
==========================

Bitbucket [seems to have abandoned](https://bitbucket.org/site/master/issue/6186/add-broker-for-trello-bb-7347  ) the development of brokers before they got around to implementing a broker for Trello.

Yet I just started my first node application and I am using Trello and Bitbucket so what am I to do?

Luckily, [Bitbucket has a POST hook](https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management) that allows you to supply a callback URL for each successful push.

This node.js server will give you a simple API with only one route that listens for the Bitbuck POST broker call.

* * *

Setting up the Server
---------------------

All you need is a place to run your node server. You will also need to setup the following configuration variables using process.env or an env-user.json config file:

process.env:

    process.env.TRELLO_KEY=<your_key>
    process.env.TRELLO_TOKEN=<your_token>
    process.env.TRELLO_BOARD_ID=<your_board_id>
    process.env.BITBUCKET_USER=<your_username>
    process.env.BITBUCKET_REPO=<your_repo_name>

env-user.json:

    {
      "trelloKey": "<your_key>",
      "trelloToken": "<your_token>",
      "trelloBoardId": "<your_board_id>",
      "bitbucketUser": "<your_username>",
      "bitbucketRepo": "<your_repo_name>"
    }

In order to get your Trello Application Key, follow [these instructions](https://trello.com/docs/gettingstarted/index.html#getting-an-application-key) in the Trello API documentation.

In order to get your Trello User Token, follow [these instructions](https://trello.com/docs/gettingstarted/index.html#getting-a-token-from-a-user) in the Trello API documentation.

To get your board id, go to the trello board you want to use and look in the url for the id:

    https://trello.com/b/<board_id>/<board_name>

* * *

Usage
-----

In your commit message, simply reference the card number (e.g. "Card #6") like so:

    Making some awesome.
    Card #6

This will be parsed by the bridge to determine which card should get the commit message as a comment.

To determine your card number, simply open the card an choose "Share an more..." on the bottom right of the card. This will show you the card number.

The result is a comment on your card for each commit. The text fo the comment will contain the commiter's name and email, the commit message, and a link back to the commit in Bitbucket.

![Sample Commit Comment](http://f.cl.ly/items/3R0V3c0m1W1v2Y010m3t/screen.jpg "Sample Commit Comment")