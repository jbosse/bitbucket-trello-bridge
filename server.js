var hapi, port, server, Trello, config;

hapi = require("hapi");
Trello = require("node-trello");
config = require("./config").init();
port = process.env.PORT || 4274;
trelloKey = process.env.TRELLO_KEY;
trelloToken = process.env.TRELLO_TOKEN;
trelloService = new Trello(config.trelloKey, config.trelloToken);
server = hapi.createServer(port);

function getCommitMessage(commit){
  var message  = "";
  message += "**" + commit.raw_author + "**";
  message += "\n\n" + commit.message;
  message += "\n\n[" + commit.node + "](https://bitbucket.org/"
             + config.bitbucketUser + "/" + config.bitbucketRepo
             + "/commits/" + commit.raw_node + ")";
  return message;
}

function getCardsReferenced(message){
  var regex = /Card #([0-9]+)/g
  var result = message.match(regex);
  var cards = [];
  if(result === null){
    console.log('No card reference found in commit:\n' + message);
    return cards;
  }
  result.forEach(function(item){
    var id = item.toLowerCase().replace("card #","");
    cards.push(id);
  });
  return cards;
}

function getCardSender(message){
  return function(id){
    sendCommitMessageToCard(id, message);
  }
}

function sendCommitMessageToCard(id, message){
  console.log("       send to card " + id);
  trelloService.get("1/boards/" + config.trelloBoardId + "/cards/" + id, function(err, card){
    console.log("       with an actual id of " + card.id);
    trelloService.post("/1/cards/" + card.id + "/actions/comments/", { text: message }, function(err, data) {
      console.log(data);
      if (err) throw err;
    });
  });
}

function payLoadSender(commit){
  var message  = getCommitMessage(commit);
  var cards = getCardsReferenced(message);
  cards.forEach(getCardSender(message));
}

function getCommits(request){
  if(request.headers['content-type'] === 'application/x-www-form-urlencoded'){
    return JSON.parse(request.payload.payload).commits;
  }
  if(request.headers['content-type'] === 'application/json'){
    return request.payload.commits;
  }
  if(request.headers['content-type'] === 'application/javascript'){
    return request.payload.commits;
  }
  return [];
}

server.route({
  method: "POST",
  path: "/trello",
  handler: function (request, reply) {
    var commits = getCommits(request);
    console.log(commits);
    console.log("attempting to send commits...");
    commits.forEach(payLoadSender);
    reply('ok');
  }
});

server.start();
